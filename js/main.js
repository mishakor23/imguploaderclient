$(document).ready(function(){
	var apiUrl = 'http://127.0.0.1:3000';
	var uploadFileEl = document.querySelector('#uf-file');
	var formEl = document.querySelector('#upload-form');
	var showAllBtn = document.querySelector('#show-all');

	showAllBtn.addEventListener('click', showAllImages);

	function showAllImages(){
		$.ajax({
			type: 'GET',
			url: apiUrl + '/api/images',
			success: function(response){
				$('.row').empty();
				var imageList = $('<div class="image-list"></div>');
				response.data.forEach(function(value){
					$('form').show();
					$('<div class="col-md-2 item">' +
						'<p id="width">Width: ' + value.width + 'px</p>' +
						'<p id="height">Height: ' + value.height + 'px</p>' +
						'<a class="image" href="#"data-id="' + value._id + '">' +
						'<img class="image-preview" src="' + apiUrl + '/' + value.path + '"></a>' +
						'</div>').appendTo(imageList);
				});
				(imageList).appendTo('.row');
			}
		});
	}



	uploadFileEl.addEventListener('change', onFileSelect);

	function onFileSelect (event) {
		$('.info').hide();
		var data = new FormData();
		data.append('image', this.files[0]);

		$.ajax({
			type: 'POST',
			url: apiUrl + '/api/images',
			data: data,
			processData: false,
			contentType: false,
			success: function (response) {
				$('<div class="col-md-2 item">'+
					'<p id="width">Width: ' + response.width + 'px</p>'+
					'<p id="height">Height: ' + response.height + 'px</p>'+
					'<a class="image" href="#" data-id="' + response.id + '">'+
					'<img class="image-preview" src="' + apiUrl + '/' + response.path + '"></a>'+
					'</div>').appendTo($('.row'));
				}
		});
	}


	$('.row').on('click', '.image', showOneImage);

	function showOneImage(event){
		event.preventDefault();
		var id = $(this).data('id');
		$.ajax({
			type: 'GET',
			url: apiUrl + '/api/images/' + id,
			success: function(response){
				$('.row').empty();
				$('form').hide();
				// $('.row').load('./templates/images.html', function(){
				// 	$('.one-image').attr('src', apiUrl +'/'+ response.path);
				// 	$('.one-image').data('id', id);
				// });
				$('<div class="text-center image-wrap">' +
				'<img class="one-image" data-id="' + id + '" src="'
				+ apiUrl + '/' + response.path + '">' +
				'<button id="rotate-img" type="button" name="button" class="btn btn-primary">' +
				'<i class="glyphicon glyphicon-repeat"></i>Rotate</button>' +
				'<button id="cropImg" type="button" name="button" class="btn btn-success">' +
				'<i class="glyphicon glyphicon-resize-small"></i>Crop</button>' +
				'<input type="number" max="1" min="0.1" step="0.1" id="size-field" class="form-control" name="size"><br>' +
				'<button id="deleteImg" type="button" name="button" class="btn btn-danger">' +
				'<i class="glyphicon glyphicon-remove"></i>Delete</button>' +
				'</div>').appendTo('.row');
			}
		})
		return false;
	}

	$('.row').on('click', '#deleteImg', deleteImage);

	function deleteImage(event){
		var id = $('#deleteImg').siblings('.one-image').data('id');
		$.ajax({
			type: 'DELETE',
			url: apiUrl + '/api/'+ id + '/delete',
			success: function(response){
				$('.row').empty();
				$('form').show();
				$('<div class="info text-center">File was deleted!</div>').appendTo('.row');
			}
		});
	}


	$('.row').on('click', '#rotate-img', rotateImage);

	function rotateImage(event){
		var id = $('#rotate-img').prev().data('id');
			$.ajax({
			type: 'POST',
			url: apiUrl + '/api/'+ id + '/rotate/',
			success: function(response){
				var hash = Math.round(Math.random()*1e10);
				$('.one-image').attr('src', apiUrl +'/'+ response.path+'?df=' + hash);
			}
		});
	}

	$('.row').on('click', '#cropImg', cropImage);

	function cropImage(){
		var id = $('#rotate-img').prev().data('id');
		var size = $('#size-field').val();
		$.ajax({
			type: 'POST',
			url: apiUrl + '/api/'+ id + '/scale/',
			data: {size: size},
			success: function(response){
				var hash = Math.round(Math.random()*1e10);
				$('.one-image').attr('src', apiUrl +'/'+ response.path+'?df=' + hash);
			}
		});
	}

});
